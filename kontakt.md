---
layout: default
title: Kontakt
description: Uns können Sie jederzeit über diese Adressangaben kontaktieren.
permalink: /kontakt/
bodyclass: kontakt
---

<div class="container">
	{% include section-title.html title="Kontakt" %}
	<div class="row">
		<div class="col-sm-6 col-sm-offset-4 vcard contact">
			<h2 class="org">Immovaleur</h2>
			<div class="adr">
				<div class="street-address">Rötschmattenweg 10</div>
				<span class="postal-code">3232 </span><span class="locality">Ins</span>
			</div>
			<div class="tel">031 972 04 77</div>
			<div class="adr">
				<div class="street-address">Pra Pury 18</div>
				<span class="postal-code">3280 </span><span class="locality">Murten</span>
			</div>
			<div class="tel">031 972 04 77</div>
			<span class="fancylink">
				<script type="text/javascript" language="javascript">
				<!--
				// Email obfuscator script 2.1 by Tim Williams, University of Arizona
				// Random encryption key feature coded by Andrew Moulden
				// This code is freeware provided these four comment lines remain intact
				// A wizard to generate this code is at http://www.jottings.com/obfuscator/
				{ coded = "fAXy@fqqy9wBIgk.Uc"
				key = "KjM1N5txGP0Orqinzobh7eZsT2aJElkufADypcdIvV63wFQBWgXLR84UHm9CSY"
				shift=coded.length
				link=""
				for (i=0; i<coded.length; i++) {
				if (key.indexOf(coded.charAt(i))==-1) {
				ltr = coded.charAt(i)
				link += (ltr)
				}
				else {     
				ltr = (key.indexOf(coded.charAt(i))-shift+key.length) % key.length
				link += (key.charAt(ltr))
				}
				}
				document.write("<a class='email' href='mailto:"+link+"'>info@immovaleur.ch</a>")
				}
				//-->
				</script><noscript>info [at] immovaleur.ch</noscript>
			</span>
		</div>
	</div>
</div>
