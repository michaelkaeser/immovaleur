---
layout: default
title: Boutique für Immobilienwerte
bodyclass: home
---

{% include section-title.html title="Boutique für Immobilienwerte" %}
<div class="row">
	<div class="col-sm-10 col-lg-8">
		<p class="text-big circle">
			To give real service, you must add something which cannot be bought or measured with money and that is sincerity and integrity.
		</p>
		<p class="author">Douglas Adams</p>
		<br>
	</div>
</div>
<br>
<section class="immos gallery">
	<div class="row">
		<div class="col-sm-12">
			<div class="carousel slide" data-ride="carousel" id="image-carousel" data-interval="10000" data-pause="null">
					<!-- Carousel Slides  -->
					<div class="carousel-inner"> 
						<!-- Image 1 -->
						<div class="item active">
							<img class="img-responsive" src="{{ site.baseurl }}{% link /media/images/frontpage/IMG_0255.jpg %}">
						</div>
						<!-- Image 2 -->
						<div class="item">
							<img class="img-responsive" src="{{ site.baseurl }}{% link /media/images/frontpage/Front2.jpg %}">
						</div>
						<!-- Image 3 -->
						<div class="item">
							<img class="img-responsive" src="{{ site.baseurl }}{% link /media/images/frontpage/10.01 5P3A5894.jpg %}">
						</div>
						<!-- Image 4 -->
						<div class="item">
							<img class="img-responsive" src="{{ site.baseurl }}{% link /media/images/frontpage/IMG_7382.jpg %}">
						</div>
					</div>
					<!-- Carousel Buttons Next/Prev -->
					<a data-slide="prev" href="#image-carousel" class="left carousel-control"><span><</span></a>
					<a data-slide="next" href="#image-carousel" class="right carousel-control"><span>></span></a>
				</div>                          
		</div>
	</div>
</section>
