---
layout: default
title: Referenzen
description: Eine Übersicht an Referenzen zu unseren Projekten.
permalink: /referenzen/
bodyclass: immobilien
---

{% include section-title.html title="Referenzen" %}
<section class="immos gallery">
	<div class="row">
		<div class="col-sm-12">
			<div class="carousel slide" data-ride="carousel" id="image-carousel" data-interval="10000" data-pause="null">
					<!-- Carousel Slides  -->
					<div class="carousel-inner"> 
						<!-- Image 1 -->
						<div class="item active">
							<img class="img-responsive" src="{{ site.baseurl }}{% link /media/images/referenzen/slides/slide-1.jpg %}">
						</div>
						<!-- Image 2 -->
						<div class="item">
							<img class="img-responsive" src="{{ site.baseurl }}{% link /media/images/referenzen/slides/slide-25.jpg %}">
						</div>
						<!-- Image 3 -->
						<div class="item">
							<img class="img-responsive" src="{{ site.baseurl }}{% link /media/images/referenzen/slides/slide-30.jpg %}">
						</div>
						<!-- Image 4 -->
						<div class="item">
							<img class="img-responsive" src="{{ site.baseurl }}{% link /media/images/referenzen/slides/slide-31.jpg %}">
						</div>
						<!-- Image 5 -->
						<div class="item">
							<img class="img-responsive" src="{{ site.baseurl }}{% link /media/images/referenzen/slides/slide-32.jpg %}">
						</div>
						<!-- Image 6 -->
						<div class="item">
							<img class="img-responsive" src="{{ site.baseurl }}{% link /media/images/referenzen/slides/slide-16.jpg %}">
						</div>
						<!-- Image 7 -->
						<div class="item">
							<img class="img-responsive" src="{{ site.baseurl }}{% link /media/images/referenzen/slides/slide-2.jpg %}">
						</div>
						<!-- Image 8 -->
						<div class="item">
							<img class="img-responsive" src="{{ site.baseurl }}{% link /media/images/referenzen/slides/slide-33.jpg %}">
						</div>
						<!-- Image 9 -->
						<div class="item">
							<img class="img-responsive" src="{{ site.baseurl }}{% link /media/images/referenzen/slides/slide-34.jpg %}">
						</div>
						<!-- Image 10 -->
						<div class="item">
							<img class="img-responsive" src="{{ site.baseurl }}{% link /media/images/referenzen/slides/slide-21.jpg %}">
						</div>
						<!-- Image 11 -->
						<div class="item">
							<img class="img-responsive" src="{{ site.baseurl }}{% link /media/images/referenzen/slides/slide-19.jpg %}">
						</div>
						<!-- Image 12 -->
						<div class="item">
							<img class="img-responsive" src="{{ site.baseurl }}{% link /media/images/referenzen/slides/slide-22.jpg %}">
						</div>
					</div>
					<!-- Carousel Buttons Next/Prev -->
					<a data-slide="prev" href="#image-carousel" class="left carousel-control"><span><</span></a>
					<a data-slide="next" href="#image-carousel" class="right carousel-control"><span>></span></a>
				</div>                          
		</div>
	</div>
</section>

