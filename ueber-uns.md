---
layout: fullwidthsection-page
title: Über uns
description: Hier einige Informationen über die Menschen von Maeder Property Partners.
permalink: /ueber-uns/
bodyclass: ueber
---

<div class="container">
	{% include section-title.html title="Über uns" %}
	<div class="row">
		<div class="col-sm-10">
			<p class="text-big">
				Unser Handwerk besteht aus dem Zusammenführen von Liegenschaft, Projektidee und Kapital.
			</p>
			<br><br><br>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-10">
			<p class="circle text-big">
				Die Immovaleur ist eine spezialisierte Boutique für Immobilienprojekte. Gemeinsam mit unseren Partnern erkennen, schaffen und erhalten wir Immobilienwerte. Wir begleiten Immobilienprojekte vom Grundstückserwerb, über die Positionierung, der Projektsteuerung, bis hin zur Vermarktung. Wir agieren vornehmlich als selbständige Immobilienentwickler, stehen aber auch als Partner für Immobilienbesitzer, Privatinvestoren, Family Offices und Institutionelle Anleger zur Verfügung.
			</p>
			<br>
		</div>
	</div>	
</div>

<div class="fullwidthsection">
	<div class="container">
		<div class="row">
			<div class="col-xs-10 col-xs-offset-1 col-sm-offset-2">
				<h1 class="section-title not-first-section">Was uns auszeichnet</h1>
			</div>
		</div>
		<div class="row">
			<ul class="col-md-4 col-xs-offset-1 col-sm-offset-2">
				<li class="text-big circle">Unternehmerische Kreativität</li>
				<li class="text-big circle">Kundenorientierte Flexibilität</li>
			</ul>
			<ul class="col-md-5 col-xs-offset-1 col-sm-offset-2 col-md-offset-0">
				<li class="text-big circle">Agilität und kurze Entscheidungswege</li>
				<li class="text-big circle">Partnerschaftliche Verlässlichkeit</li>
			</ul>
		</div>
	</div>
</div>

<div id="team" class="container">
	{% include section-title.html title="Verwaltungsrat" class="not-first-section" %}
	<div class="row">
		<div class="col-sm-5 col-md-4 col-lg-3 text-center">
			<img src="{{ site.baseurl }}{% link /media/images/portraits/j-maeder-4.jpg %}" alt="Portrait von Jonathan Maeder.">
			<p class="text-big">
				Jonathan Maeder<br>
				<em>Gründer & VRP</em>
				<br>
				<span>MAS Real Estate Management FH</span>
				<br>
				<br>
				<span>Spezialgebiet: Akquisition & Verkauf</span>
				<br>
				<span>Mitglied Schweizer Immobilienschätzer-Verband</span>
			</p>
		</div>
		<div class="col-sm-5 col-md-4 col-lg-3 text-center">
			<img src="{{ site.baseurl }}{% link /media/images/portraits/s-cordey-2.jpg %}" alt="Portrait von Jonathan Maeder.">
			<p class="text-big">
				Sylvain Cordey<br>
				<em>Delegierter des Verwaltungsrates</em>
				<br>
				<span>Dipl. Arch ETH SIA</span>
				<br>
				<br>
				<span>Spezialgebiet: Entwicklung & Kostenkontrolle</span>
				<br>
				<span>Mitglied Schweizerischer Ingenieur- und Architektenverein</span>
			</p>
		</div>
	</div>
</div>
