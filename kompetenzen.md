---
layout: default
title: Kompetenzen
description: Projektentwicklung - Bewertung - Vermarktung - Verwaltung - Ankauf
permalink: /kompetenzen/
bodyclass: dienstleistungen
---

{% include section-title.html title="Kompetenzen" class="wordwrap" %}
<div class="row">
	<div class="col-sm-10 col-lg-8">
		<p class="text-big">
			Unsere Leidenschaft für Immobilien stellen wir auch Dritten als Dienstleistung zur Verfügung. Als Besitzer und Entwickler von eigenen Immobilien wissen wir welche Rezepte zum Erfolg führen. Profitieren auch Sie von unserer vielfältigen Erfahrung.
		</p>
		<br><br><br>
	</div>
</div>
<div class="row">
	<div class="col-sm-4">
		<h2 class="circle sub-title">Projektentwicklung</h2>
		<h3>Immobilienwerte schaffen</h3>
	</div>
	<div class="col-sm-8">
		<br>
		<p>
			Besitzen Sie eine Liegenschaft mit Potential? In den vergangenen Jahren haben wir in den Bereichen der Projektentwicklung und der Strukturierung von Immobilieninvestitionen ein unverkennbares Wissen aufgebaut. Gerne stellen wir für erfolgsversprechende Vorhaben unser Know-How und insbesondere unser Netzwerk zur Verfügung.
		</p>
	</div>
</div>		
<br><br>
<div class="row">
	<div class="col-sm-4">
		<h2 class="circle sub-title">Vermarktung</h2>
		<h3>Immobilienwerte realisieren</h3>
	</div>
	<div class="col-sm-8">
		<br>
		<p>
			Sind Sie bereits Besitzer einer Liegenschaft? Wir führen den Verkauf oder die Erstvermietung zum Erfolg. Als Käufer einer von uns entwickelten Liegenschaften, profitieren Sie des Weiteren von attraktiven Sonderkonditionen auf dem Verkaufsmandat. 
		</p>
	</div>
</div>		
<br><br>
<div class="row">
	<div class="col-sm-4">
		<h2 class="circle sub-title">Bewertung</h2>
		<h3>Immobilienwerte erkennen</h3>
	</div>
	<div class="col-sm-8">
		<br>
		<p>
			Möchten Sie wissen, wieviel Ihre Liegenschaft heute auf dem Markt wert ist? Interessiert Sie, ob Ihre Liegenschaft wertsteigernd umgenutzt werden kann? Als ausgebildete Immobilien-Bewerter berechnen wir anhand von international anerkannten Methoden, den Wert Ihrer Liegenschaft und zeigen verschiedene zukunftsorientierte sowie wertsteigernde Optionen auf.  
		</p>
	</div>
</div>		
<br><br>
<div class="row">
	<div class="col-sm-4">
		<h2 class="circle sub-title">Verwaltung</h2>
		<h3>Immobilienwerte erhalten</h3>
	</div>
	<div class="col-sm-8">
		<br>
		<p>
			Nur wer nahe am Marktgeschehen ist, kennt die Bedürfnisse von Mietern und anderen Anspruchsgruppen. Die Verwaltung unserer Immobilien nehmen wir deshalb in die eigene Hand. Gerne stellen wir unsere Expertise auch externen Partnern zur Verfügung.  
		</p>
	</div>
</div>		
	